import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.*;

public class kwic {

    public String convert(String txt) {
        String[] txtlist = txt.split("::");
        String keylist = txtlist[0];
        String sentencelist = txtlist[1].substring(1);

        String[] key = keylist.split("\n");
        ArrayList<String> keyAL = new ArrayList<>(List.of(key));
        String[] sentence = sentencelist.split("\n");
        //to lower case
        for (int i = 0; i < sentence.length; i++) {
            sentence[i] = sentence[i].toLowerCase();
        }
        ArrayList<String> upperKey = new ArrayList<>();
        ArrayList<String> converted_sentence = new ArrayList<>();
        for (int i = 0; i < sentence.length; i++) {
            ArrayList<String> word = new ArrayList<String>(List.of(sentence[i].split(" ")));
            for (int j = 0; j < word.size(); j++) {
                if (!keyAL.contains(word.get(j).toLowerCase())) {
                    StringBuilder tempsentSB = new StringBuilder();
                    upperKey.add(word.get(j));
                    for (int k = 0; k < word.size(); k++) {

                        if (k != j) {

                            tempsentSB.append(word.get(k).toLowerCase());
                            tempsentSB.append(" ");
                        } else {
                            tempsentSB.append(word.get(k).toUpperCase());
                            tempsentSB.append(" ");
                        }
                    }
                    tempsentSB.deleteCharAt(tempsentSB.length() - 1);
                    String tempsent = tempsentSB.toString();

                    converted_sentence.add(tempsent);
                }
            }
        }

        HashMap<String, ArrayList<String>> M = new HashMap<>();
        for (int i = 0; i < converted_sentence.size(); i++) {
            if (!M.containsKey(upperKey.get(i))){
                M.put(upperKey.get(i), new ArrayList<>());
            }
            M.get(upperKey.get(i)).add(converted_sentence.get(i));
        }

        Map<String, ArrayList<String>> M_sort = new TreeMap<>(M);

        //output
        StringBuilder output = new StringBuilder();
        for (Map.Entry<String, ArrayList<String>> entry : M_sort.entrySet()) {
            for (int index = 0; index < entry.getValue().size(); index++){
                output.append(entry.getValue().get(index));
                output.append("\n");
            }


        }
        output.deleteCharAt(output.length()-1);


        return output.toString();
    }

    public static void main(String args[]) {
        String txt = "is\n" +
                "the\n" +
                "of\n" +
                "and\n" +
                "as\n" +
                "a\n" +
                "but\n" +
                "::\n" +
                "Descent of Man\n" +
                "The Ascent of Man\n" +
                "The Old Man and The Sea\n" +
                "A Portrait of The Artist As a Young Man\n" +
                "A Man is a Man but Bubblesort IS A DOG";
        new kwic().convert(txt);
    }
}
